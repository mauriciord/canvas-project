import React, { useCallback } from 'react';

import { useFabric } from '../../../shared/hooks';
import { Row, Slider, Picture, SelectableFilter, ButtonGroup, ButtonSave, ButtonReset } from '../styles';
import ASSETS from "../constants/assets";

function ImagePreview({
  onHandleChange,
  onHandleSubmit,
  filterStrength,
  pictureUrl,
  setFieldValue,
  selectedFilter,
  imageExtension,
  handleReset,
}) {
  const [parentRow, fabricRef] = useFabric({ pictureUrl, selectedFilter, filterStrength });

  const handleSetFilter = useCallback((filterName) => () => setFieldValue('selectedFilter', filterName), [setFieldValue]);

  const handleSaveImage = useCallback((extension, canvasRef) => () => {
    setFieldValue('imageExtension', extension);
    setFieldValue('canvasRef', canvasRef);
    onHandleSubmit();
  }, [onHandleSubmit, setFieldValue]);

  const handleFormReset = useCallback(() => {
    if (window.confirm('Are you sure?')) {
      handleReset();
    }
  }, [handleReset]);

  return (
    <>
      <Row>
        <label htmlFor="filterStrength">Filter Strength</label>
        <Slider
          type="range"
          min="0"
          step="10"
          max="100"
          value={filterStrength}
          onChange={onHandleChange}
          id="filterStrength"
          name="filterStrength"
        />
      </Row>
      <Row ref={parentRow}>
        <Picture ref={fabricRef} width={200} height={200} />
      </Row>
      <Row justify="space-around" maxWidth={960} alignItems="stretch">
        <SelectableFilter onClick={handleSetFilter('none')}>
          <img src={pictureUrl} alt="none" />
          <span>Original</span>
        </SelectableFilter>
        <SelectableFilter onClick={handleSetFilter('Sepia')}>
          <img src={ASSETS.SEPIA_PORTRAIT} alt="sepia" />
          <span>Sepia</span>
        </SelectableFilter>
        <SelectableFilter onClick={handleSetFilter('Vintage')}>
          <img src={ASSETS.VINTAGE_PORTRAIT} alt="vintage" />
          <span>Vintage</span>
        </SelectableFilter>
        <SelectableFilter onClick={handleSetFilter('Blur')}>
          <img src={ASSETS.BLUR_PORTRAIT} alt="blur" />
          <span>Blur</span>
        </SelectableFilter>
      </Row>
      <Row justify="space-around">
        <ButtonReset onClick={handleFormReset}>
          Reset
        </ButtonReset>
        <ButtonGroup>
          <ButtonSave onClick={handleSaveImage('jpeg', fabricRef)}>Save as JPEG</ButtonSave>
          <ButtonSave onClick={handleSaveImage('png', fabricRef)}>Save as PNG</ButtonSave>
        </ButtonGroup>
      </Row>
    </>
  );
};

export default ImagePreview;
