import React, { useCallback } from 'react';
import { useFormik } from 'formik';

import { Row, Form, InputPicture, LabelButton } from '../styles';
import ImagePreview from './ImagePreview';
import { useSafeState } from '../../../shared/hooks';

const FiltersForm = () => {
  const [state, setSafeState] = useSafeState({
    pictureUrl: undefined,
  });
  const formik = useFormik({
    initialValues: {
      picture: null,
      filterStrength: 50,
      selectedFilter: 'none',
      imageExtension: 'jpg',
      canvasRef: null,
    },
    onSubmit: ({ imageExtension, canvasRef }) => {
      const link: HTMLAnchorElement = document.createElement('a');
      link.href =
        // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
        // @ts-ignore
        canvasRef.current.toDataURL({
          format: imageExtension === 'jpg' ? 'jpeg' : imageExtension,
          enableRetinaScaling: true,
        });
      link.setAttribute('download', `image.${imageExtension}`);

      document.body.appendChild(link);
      link.click();
      // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
      // @ts-ignore
      link.parentNode.removeChild(link);
    },
  });

  const {
    setFieldValue,
    handleChange,
    handleSubmit,
    values,
    handleReset,
  } = formik;

  const handlePictureChange = useCallback(
    (event) => {
      event.preventDefault();

      const reader = new FileReader();
      const file = event.target.files[0];

      reader.onloadend = () => {
        setSafeState({
          pictureUrl: reader.result,
        });
        setFieldValue('picture', file);
      };

      reader.readAsDataURL(file);
    },
    [setFieldValue, setSafeState],
  );

  const { picture, selectedFilter, filterStrength, imageExtension } = values;

  return (
    <Form onSubmit={handleSubmit}>
      {picture ? (
        <ImagePreview
          onHandleChange={handleChange}
          onHandleSubmit={handleSubmit}
          pictureUrl={state.pictureUrl}
          setFieldValue={setFieldValue}
          selectedFilter={selectedFilter}
          filterStrength={filterStrength}
          imageExtension={imageExtension}
          handleReset={handleReset}
        />
      ) : (
        <Row>
          <InputPicture
            id="picture"
            name="picture"
            type="file"
            onChange={handlePictureChange}
            accept="image/*"
          />
          <LabelButton htmlFor="picture">Choose an image</LabelButton>
        </Row>
      )}
    </Form>
  );
};

export default FiltersForm;
