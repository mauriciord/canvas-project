import styled from 'styled-components';

type Row = {
  justify?: string;
  maxWidth?: number;
  alignItems?: string;
};

export const InputPicture = styled.input`
  border: 0;
  clip: rect(0, 0, 0, 0);
  height: 1px;
  overflow: hidden;
  padding: 0;
  position: absolute !important;
  white-space: nowrap;
  width: 1px;
  background-color: #529fff;
  border-radius: 0.5rem;
  color: #fff;
  cursor: pointer;
  display: inline-block;
  padding: 1rem;
`;

export const LabelButton = styled.label`
  background-color: #529fff;
  border-radius: 0.5rem;
  color: #fff;
  cursor: pointer;
  display: inline-block;
  padding: 1rem;
`;

export const Form = styled.form`
  display: flex;
  align-self: stretch;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  flex: 1;
`;

export const Row = styled.div`
  display: flex;
  flex-direction: row;
  align-self: ${(p: Row) => (p.maxWidth ? 'center' : 'stretch')};
  justify-content: ${(p: Row) => p.justify || 'center'};
  align-items: ${(p: Row) => p.alignItems || 'center'};
  ${(p: Row) =>
    p.maxWidth &&
    `
    width: 100%;
    max-width: ${p.maxWidth}px;
  `}
`;

export const Slider = styled.input`
  -webkit-appearance: none;
  width: 100%;
  height: 25px;
  background: #d3d3d3;
  outline: none;
  opacity: 0.7;
  -webkit-transition: 0.2s;
  transition: opacity 0.2s;
  border-radius: 0.5rem;

  &: hover {
    opacity: 1;
  }

  &::-webkit-slider-thumb {
    -webkit-appearance: none;
    appearance: none;
    width: 25px;
    height: 25px;
    background: #529fff;
    cursor: pointer;
    border-radius: 0.5rem;
  }
  &::-moz-range-thumb {
    width: 25px;
    height: 25px;
    background: #529fff;
    cursor: pointer;
    border-radius: 0.5rem;
  }
`;

export const SelectableFilter = styled.div`
  width: 20%;
  max-width: 120px;
  border: 1px dashed #ccc;
  padding: 3px;
  border-radius: 0.5rem;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: space-between;

  > img {
    width: 100%;
    max-width: 120px;
    height: 100%;
    max-height: 158px;
    border-radius: 0.4rem;
  }

  > span {
    color: #505165;
    font-size: 0.9rem;
    font-weight: bold;
    height: 60px;
    display: flex;
    align-items: center;
    justify-content: center;
    align-self: center;
  }
`;

export const Picture = styled.canvas`
  margin: 5px;
`;

export const ButtonGroup = styled.div`
  margin: 10px 0;

  &:after {
    content: '';
    clear: both;
    display: table;
  }
`;

export const ButtonSave = styled.button.attrs(() => ({
  type: 'button',
}))`
  background-color: #529fff;
  border: 1px solid blue;
  color: white;
  padding: 10px 24px;
  cursor: pointer;
  float: left;

  &:not(:last-child) {
    border-right: none;
  }
  &:first-child {
    border-top-left-radius: 0.4rem;
    border-bottom-left-radius: 0.4rem;
  }
  &:last-child {
    border-top-right-radius: 0.4rem;
    border-bottom-right-radius: 0.4rem;
  }

  &:hover {
    background-color: #1097ff;
  }
`;

export const ButtonReset = styled.button.attrs(() => ({
  type: 'button',
}))`
  background-color: #bca1bf;
  color: black;
  padding: 10px 24px;
  cursor: pointer;
  border-radius: 0.4rem;
`;
