import sepia from '../../../assets/sepia.jpg';
import vintage from '../../../assets/vintage.jpg';
import blur from '../../../assets/blur.jpg';

const ASSETS = {
  BLUR_PORTRAIT: blur,
  SEPIA_PORTRAIT: sepia,
  VINTAGE_PORTRAIT: vintage,
};

export default ASSETS;
