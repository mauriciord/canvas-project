import React from 'react';
import { Normalize } from 'styled-normalize';

import { FiltersForm } from './features/picture';
import { Body, Wrapper } from './shared/components/styles';

const App = () => {
  return (
    <>
      <Normalize />
      <Body>
        <Wrapper>
          <FiltersForm />
        </Wrapper>
      </Body>
    </>
  );
};

export default App;
