import styled from 'styled-components';

export const Body = styled.div`
  background: #fff;
  display: flex;
  flex-direction: column;
  width: 100vw;
  height: 100vh;
`;

export const Wrapper = styled.div`
  background: #fff;
  display: flex;
  flex-direction: column;
  flex: 1;
  align-self: stretch;
  border: 1px dashed #ccc;
  padding: 5px;
  margin: 10px;
`;
