export const calcPercentage = (image, offsetWidth) =>
  Math.floor(image.width * 0.4) > offsetWidth
    ? Math.floor((offsetWidth * 100) / image.width) / 100
    : 0.4;
export const calcImgWidth = (image, percentage) =>
  Math.floor(image.width * percentage);
export const calcImgHeight = (image, percentage) =>
  Math.floor(image.height * percentage);
