import { useRef, useEffect } from 'react';
import { fabric } from 'fabric';

import {
  calcPercentage,
  calcImgHeight,
  calcImgWidth,
} from '../helpers/imageCalculations';

let fbCanvas = null;

function useFabric({ pictureUrl, selectedFilter, filterStrength }) {
  const parentRow = useRef();
  const fabricRef = useRef();

  useEffect(() => {
    const node = fabricRef.current;
    const fabricCanvas = new fabric.Canvas(node);
    const { offsetWidth } = parentRow.current;

    fabric.Image.fromURL(pictureUrl, (img) => {
      const percentage = calcPercentage(img, offsetWidth - 10);
      // -10 because of margin rigth and margin left
      const imageWidth = calcImgWidth(img, percentage);
      const imageHeight = calcImgHeight(img, percentage);

      // scale image
      img.scaleToWidth(imageWidth);

      if (selectedFilter !== 'none') {
        // add filter
        img.filters.push(
          new fabric.Image.filters[selectedFilter]({
            ...(selectedFilter === 'Blur' && {
              blur: filterStrength / 100,
            }),
          }),
        );

        // apply filters and re-render canvas when done
        img.applyFilters();
      }

      // add image onto canvas (it also re-render the canvas)
      fabricCanvas.add(img);
      fabricCanvas.setWidth(imageWidth);
      fabricCanvas.setHeight(imageHeight);

      fbCanvas = fabricCanvas;
    });

    return () => {
      fabricCanvas.dispose();
    };
  }, [fabricRef, parentRow, pictureUrl, selectedFilter, filterStrength]);

  return [parentRow, fabricRef, fbCanvas];
}

export default useFabric;
